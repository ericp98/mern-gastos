import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import './App.css';

//Components
//import Graficos from './components/graficos/Graficos'
import Ingresos from './components/Ingresos-Gastos'
import Dashboard from './components/graficos/Dashboard'

function App() {
  return (
    <div className='root'>
      <Router>
        <Route exact path='/inicio' component={Dashboard} /> 
        <Route exact path='/ingresos' component={Ingresos} />
        <Route exact path='/gastos' component={Ingresos} />
      </Router>
    </div>
  );
}

export default App;
