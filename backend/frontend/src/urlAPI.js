function getUrl(){
    const urlDev = 'http://localhost:4000';
    const urlProduction = 'https://react-gastos-ep.herokuapp.com'
    return (window.location.origin === urlProduction ? urlProduction : urlDev) ; 
}

module.exports.getUrl = getUrl; 