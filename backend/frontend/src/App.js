import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import './App.css';
import Cookies from 'universal-cookie';


//Components
import Ingresos from './components/Ingresos-Gastos';
import Dashboard from './components/graficos/Dashboard';
import FormularioUsers from './components/Formulario-Users';
import Settings from './components/Settings';

function App() {

  const cook = new Cookies()
  const [cookie, setCookie] = React.useState(cook.get('user'));

  return (
    <div className='root'>
        <Router>
          <Switch>
            <Route exact path='/'>
              {cookie != null ? <Redirect to='/inicio' /> : <FormularioUsers route='login' handleLoggin={() => setCookie('user')} />}
            </Route> 

            <Route exact path='/inicio'>
              {cookie != null ? <Dashboard idUserLogged={cookie}/> : <FormularioUsers route='login' handleLoggin={() => setCookie('user')}/>}
            </Route>

            <Route exact path='/ingresos'> 
              {cookie != null ? <Ingresos route='ingresos' /> : <FormularioUsers route='login' handleLoggin={() => setCookie('user')}/> }
            </Route>

            <Route exact path='/gastos'> 
              {cookie != null ? <Ingresos route='gastos'  /> : <FormularioUsers route='login' handleLoggin={() => setCookie('user')}/> }
            </Route>

            <Route exact path='/settings'> 
              {cookie != null ? <Settings /> : <FormularioUsers route='login' handleLoggin={() => setCookie('user')}/> }
            </Route>

            <Route exact path='/register'> 
              <FormularioUsers route='register'/>
            </Route>

          </Switch>
        </Router>
    </div>
  );
}

export default App;
