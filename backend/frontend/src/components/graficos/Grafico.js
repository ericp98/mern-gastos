import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2'

export default class Grafico extends Component {

    render() {
        const graficos = this.props.graficos
        const isChartEmpty = this.props.isChartsEmpty

        return (
            !isChartEmpty ? // Si el grafico no esta vacio
            <div>

                <div className="row justify-content-center">   

                    {graficos.map((chart) => 
                        chart.labels.length > 0 ?
                        <div className={"col-md-8 col-lg-5 "} key={chart.key}>

                            {/* Pie chart */}
                            <div className={"card shadow mb-4"}>

                                {/* Card header */}
                                <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 className="m-0 font-weight-bold text-primary">{chart.titulo}</h6>
                                </div>

                                    {/* Card body */}
                                    <div className="card-body">
                                        
                                        <Doughnut  
                                            data={chart}
                                            options={{
                                                title: {
                                                    display:false
                                                },
                                                legend:{
                                                    display: true
                                                    , position: 'bottom'
                                                },
                                                    
                                            }}
                                        />
                                                                          
                                </div>
                            </div>
                        </div>
                        : null
                    )}     

                </div>
            </div>

            : // Si el grafico esta vacio

            <div className="row justify-content-center">   

                     
                        <div className={"col-md-8 col-lg-5 "}>

                            {/* Pie chart */}
                            <div className={"card shadow mb-4"}>

                                {/* Card header */}
                                <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 className="m-0 font-weight-bold text-primary">Mensaje</h6>
                                </div>

                                    {/* Card body */}
                                    <div className="card-body">
                                        Debe insertar al menos un concepto de Ingresos y/o de Gastos para visualizar los graficos                                                                     
                                </div>
                            </div>
                        </div>
                         

                </div>
        )        
    }
}
