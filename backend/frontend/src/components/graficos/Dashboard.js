import React, { Component } from 'react'
import Graficos from '../graficos/Graficos'
import Cards from '../graficos/Cards'
import axios from 'axios';
import Sidebar from '../Sidebar'
import Cookies from 'universal-cookie';
const urlAPI = require('../../urlAPI');

export default class Dashboard extends Component {

    state = {
        // Cards
        ingresoMensualARS: ''
        , ingresoMensualUSD: ''
        , gastoMensualARS: ''
        , gastoMensualUSD: ''
    }

    async getData(routeAPI, stateName) {
        const res = await axios.get(routeAPI);

        switch(stateName){
            case ('ingresoMensualARS'):
                this.setState({ingresoMensualARS: this.getMonto(res.data)});
                break;
                
            case ('ingresoMensualUSD'):
                this.setState({ingresoMensualUSD: this.getMonto(res.data)});
                break;

            case ('gastoMensualARS'):
                this.setState({gastoMensualARS: this.getMonto(res.data)});
                break;

            case ('gastoMensualUSD'):
                this.setState({gastoMensualUSD: this.getMonto(res.data)});
                break;
                
            default: alert('Error al cargar los datos')
        }
    }

    getMonto(data){
        if (data.length > 0){
            return (data[0].monto)
        } else{
            return 0
        }
    }


    componentDidMount(){

        const cookie = new Cookies();
        const cook = cookie.get('user');
        const idUser = cook;

        this.getData((urlAPI.getUrl() + '/api/ingresos/ARS/') + idUser, 'ingresoMensualARS');
        this.getData((urlAPI.getUrl() + '/api/ingresos/USD/') + idUser, 'ingresoMensualUSD');
        this.getData((urlAPI.getUrl() + '/api/gastos/ARS/') + idUser, 'gastoMensualARS');
        this.getData((urlAPI.getUrl() + '/api/gastos/USD/') + idUser, 'gastoMensualUSD'); 
    }  


    render() {
        const montos = [
            ["ingresos ARS", '$ ' + this.state.ingresoMensualARS]
            , ["gastos ARS", '$ ' + this.state.gastoMensualARS]
            , ["ingresos USD", 'USD ' + this.state.ingresoMensualUSD]
            , ["gastos USD", 'USD ' + this.state.gastoMensualUSD]
        ]

        //const route = this.props.match.path.replace('/','');
        const route = 'inicio'

        return (
             
            <div>
                <Sidebar 
                route = {route}
                componente={[
                    <Cards montos={montos} key={'1'} />
                    , <Graficos 
                        key={'2'}
                        idUserLogged={this.props.idUserLogged}
                      />
                ]} 
                />
            </div>
        )
    }
}
