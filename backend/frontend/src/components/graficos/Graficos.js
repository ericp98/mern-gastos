import React, { Component } from 'react';
import Grafico from './Grafico';
import axios from 'axios';
import Cookies from 'universal-cookie';
const urlAPI = require('../../urlAPI');

export default class Graficos extends Component {

    state = {
        ingresos_ARS: []
        , ingresos_USD: []
        , gastos_ARS: []
        , gastos_USD: []
        , saldo_ARSUSD: []
    }
    
    componentDidMount(){

        const cookies = new Cookies();
        const cook = cookies.get('user');
        
        this.getIngresosARS(cook);
        this.getIngresosUSD(cook);
        this.getGastosARS(cook);
        this.getGastosUSD(cook);
        this.getSaldoARSUSD(cook);
    }

    async getIngresosARS(cook){
        const ingresos_ARS = await axios.get(urlAPI.getUrl() + '/api/ingresos/ingresos_ARS/' + cook);
        this.setState({ingresos_ARS: ingresos_ARS.data}, () => console.log(cook))
    }

    async getIngresosUSD(cook){
        const ingresos_USD = await axios.get(urlAPI.getUrl() + '/api/ingresos/ingresos_USD/' + cook);
        this.setState({ingresos_USD: ingresos_USD.data})
    }

    async getGastosARS(cook){
        const gastos_ARS = await axios.get(urlAPI.getUrl() + '/api/gastos/gastos_ARS/' + cook);
        this.setState({gastos_ARS: gastos_ARS.data})
    }

    async getGastosUSD(cook){
        const gastos_USD = await axios.get(urlAPI.getUrl() + '/api/gastos/gastos_USD/' + cook);
        this.setState({gastos_USD: gastos_USD.data})
    }

    async getSaldoARSUSD(cook){
        const saldo_ARSUSD = await axios.get(urlAPI.getUrl() + '/api/datos_financieros/datosf/' + cook);
        this.setState({saldo_ARSUSD: saldo_ARSUSD.data})
    }

    chartData(estado, indice){
        const labels = this.getLabels(estado[0]);
        const montos = this.getMontos(estado[0], estado[1]);
        
        //Diferencia la data a graficar
        // 0 = Grafico Gastos-Ingresos - 1 = Grafico saldo ARS-USD
        const titulo = estado[1] 
        const key = indice

        // Genero colores dinamicamente, por cada label
        /* let colors = []
        for (let i=0; labels.length; i++){
            colors[i] = this.getColor(); 
            console.log((Math.random() * 255).toFixed(0))
        }  */

        if (titulo !== "Saldo ARS - USD"){
            return ({
                labels: labels,
                titulo: titulo,
                key: key,
                datasets:[
                    {
                        label: 'Population',
                        data: montos,
                        backgroundColor: [
                            '#9b59b6',
                            '#2980b9',
                            '#e67e22',
                            '#1abc9c',
                            '#f39c12',
                            '#2980b9',
                            '#1abc9c',
                            '#c0392b',
                        ]
                    }
                ]
            }) 
        } else {
            return ({
                labels: ["Saldo ARS", "Saldo USD"],
                key: key,
                titulo: titulo,
                datasets:[
                    {
                        label: 'Population',
                        data: montos,
                        backgroundColor: [
                            '#9b59b6',
                            '#2980b9',
                            '#e67e22',
                            '#1abc9c',
                            '#f39c12',
                            '#2980b9',
                        ]
                    }
                ]
            })
        }
    }

    /* getColor(){
        const r = Math.floor(Math.random() * 255); 
        const g = Math.floor(Math.random() * 255); 
        const b = Math.floor(Math.random() * 255); 
        return ("rgb(" + r + "," + g + "," + b + ")");
    }  */

    getLabels(estado){
        const labels = [];

        for (let i = 0; i < estado.length; i++){
            labels[i] = estado[i]._id
        }

        return (labels)
    }

    getMontos(estado, titulo){
        const montos = [];

        if (titulo !== "Saldo ARS - USD"){
            for (let i = 0; i < estado.length; i++){
                montos[i] = estado[i].monto
            }
        } else {
            for (let i = 0 ; i < estado.length ; i++){
                montos[i] = estado[i].saldo_ARS
                montos[i+1] = estado[i].saldo_USD
            }
        }

        return (montos)
    }

    render() {

        const ingresos_ARS = this.state.ingresos_ARS
        const ingresos_USD = this.state.ingresos_USD
        const gastos_ARS = this.state.gastos_ARS
        const gastos_USD = this.state.gastos_USD
        const saldo_ARSUSD = this.state.saldo_ARSUSD

        
        const graficos = [[ingresos_ARS, "Ingresos ARS"]
        , [ingresos_USD, "Ingresos USD"]
        , [gastos_ARS, "Gastos ARS"]
        , [gastos_USD, "Gastos USD"]
        , [saldo_ARSUSD, "Saldo ARS - USD"]
    ]
    const charts = []
    
        for(let i = 0; i < graficos.length; i++){
            charts[i] = this.chartData(graficos[i], i)
        }

        const isChartsEmpty = (ingresos_ARS.length < 1 && ingresos_USD.length < 1 && gastos_ARS.length < 1 && gastos_USD.length < 1)
        
        return (
            <div>
                <Grafico graficos={charts} isChartsEmpty={isChartsEmpty}/>
            </div>
        )
    }
}
