import React, { Component } from 'react'
import { FaDollarSign } from 'react-icons/fa'

export default class Cards extends Component {

    getItemColor(item){
        switch(item){
            case ("ingresos ARS"):
                return "primary"
            
            case("gastos ARS"):
                return "danger"

            case("ingresos USD"):
                return "success"

            case("gastos USD"):
                return "warning"

            default: console.log("No se cargó correctamente la informacion")
        }
    }


    render() {
        const dataCard = this.props.montos
        return (
            <div>
                <div className="container-fluid">
                    <div className="row">

                        {dataCard.map((item) => 
                            <div className="col-xl-3 col-md-6 mb-4" key={item[0]}>
                                <div className={"card shadow h-100 mt-2 py-2 border-left-" + this.getItemColor(item[0])}>
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                            <div className={"text-xs font-weight-bold text-uppercase mb-1 text-" + this.getItemColor(item[0])}>{item[0] + ' - Mensual'}</div>
                                            <div className="h5 mb-0 font-weight-bold text-gray-800">{item[1]}</div>
                                            </div>
                                            <div className="col-auto">
                                            <i className="iconsCard text-primary-300 "><FaDollarSign size={'2.0em'}/></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )} 
                    </div>
                </div>

            </div>
        )
    }
}
