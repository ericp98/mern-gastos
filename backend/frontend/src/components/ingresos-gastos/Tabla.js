import React, { Component } from "react";
import axios from 'axios';
import DataTable from 'react-data-table-component';
import { MdRefresh, MdSearch, MdDelete, MdEdit} from 'react-icons/md';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Ejemplotabla from './ejemplotabla'
var moment = require('moment');
const urlAPI = require('../../urlAPI');


export default class Tabla extends Component {

    state = {
        ingresos: [],
        ingresosBackup: [],
        textBuscar: '',
        idEdit: ''
    }

    componentDidMount(){
      this.getList();
    }

    async getList(){
      if (this.props.prop === 'ingresos'){
        const res = await axios.get(urlAPI.getUrl() + '/api/ingresos'); 
        this.setState({ingresos: res.data});
        this.setState({ingresosBackup: res.data});
      } else {
        const res = await axios.get(urlAPI.getUrl() + '/api/gastos');
        this.setState({ingresos: res.data});
        this.setState({ingresosBackup: res.data});
      }
    }

    // Tema personalizado para la tabla
    

    // Columnas para usar en la tabla
    columnas = [
      {
        name: "Concepto",
        selector: "concepto",
        sortable: true
      },

      {
        name: "Monto",
        selector: "monto",
        sortable: true
      },

      {
        name: "Moneda",
        selector: "moneda",
        sortable: true
      },

      {
        name: "Fecha",
        selector: "dateFormat",
        sortable: true
      },

      {
        name: "Conversion a dolar",
        selector: "conversion_dolar",
        sortable: true
      },

      {
        name: "Cotizacion Dolar",
        selector: "cotizacion_dolar",
        sortable: true
      },

      {
        name: "",
        selector: "iconDelete",
        sortable: true
      },

      {
        name: "",
        selector: "iconEdit",
        sortable: true
      }
    ] 

    paginacionOptions = {
      rowsPerPageText: 'Filas por pagina',
      rangeSeparatorText: 'de',
      selectAllRowsItem: true,
      selectAllRowsItemText: 'All'
    } 

    buttonRefresh(){
      return (
          <MdRefresh 
            style={{color: '#88888A', marginLeft: '10px'}} 
            size={'1.5em'}
            className="buttonHeaderTab"
            id="btnRefresh"
            onClick={() => this.getList()}
            key={'1'}
          />
      )
    };

    buttonSearch(){
      return (
        <MdSearch
          style={{marginLeft:'10px', color:'#88888A'}}
          size={'1.5em'}
          className='buttonHeaderTab'
          onClick={() => this.searching()}
          key={'2'}
        />
      )
    }

    barSearch(){
      return(
        <input 
          type="text" 
          placeholder="Buscar"
          id="search" 
          value={this.state.text} 
          onChange={(text) => this.filter(text)} 
          key="barSearchTable"
        />
      )
    }

    // Componentes a mostrar en el header de la tabla
    componente = [
      this.barSearch(),
      this.buttonSearch(),
      this.buttonRefresh()
    ]

    // Filtro de busqueda
    filter(e){

      // Obtener texto de busqueda
      var text = e.target.value;

      // Obtener datos de array
      const data = this.state.ingresosBackup;

      const newData = data.filter( (ingreso) => {
        // Obtener concepto
        const itemDataConcepto = ingreso.concepto.toUpperCase();
        // Obtener monto
        const itemDataMonto = ingreso.monto;
        const itemDataMoneda = ingreso.moneda.toUpperCase();
        const itemDataDate = ingreso.date;

        const itemData = itemDataConcepto + " " + itemDataMonto + " " + itemDataMoneda + " " + itemDataDate
        // Texto a buscar
        const textData = text.toUpperCase();
        // filtrar si existe
        return itemData.indexOf(textData) > -1
      })

      this.setState({
        ingresos: newData,
        textBuscar: text
      })
    }

  buttonEdit(item){
    const { handleChange } = this.props;
    return (
      <MdEdit 
        style={{color: '#88888A'}} 
        size={'1.5em'}
        className="buttonHeaderTab"
        id={item._id}
        onClick={() => handleChange(item, 'seleccionado')}
      />
    );
  }

  buttonDelete(id){
    return (
      <MdDelete
        style={{color: '#88888A'}} 
        size={'1.5em'}
        className="buttonHeaderTab"
        pepe={id}
        onClick={() => this.deleteData(id)}
      />
    );
  }
  
  async deleteData(id){
    if (this.props.prop === 'ingresos'){
      await axios.delete(urlAPI.getUrl() + '/api/ingresos/' + id); 
      this.getList();
    } else {
      await axios.delete(urlAPI.getUrl() + '/api/gastos/' + id); 
      this.getList();
    } 
    this.props.toastAlert('eliminado');
  }

  getData(){
    // Aniade un icono a cada elemento del JSON tomado de la API
    const data = this.state.ingresos;
    for (let i of data){
      const fecha2 = moment(i.date).format("DD/MM/YYYY");
      i.iconEdit = this.buttonEdit(i);
      i.iconDelete = this.buttonDelete(i._id);
      i.dateFormat = fecha2;
    }
    return (data);
  }

  render() {
    return (
      <div className="table-responsive wrapper">

      <DataTable 
        columns={this.columnas}
        data={this.getData()}
        pagination
        paginationComponentOptions={this.paginacionOptions}
        noHeader 
        theme="dark" 
        className="tabladata"
        //selectableRows
        //selectableRowsNoSelectAll
        dense
        subHeader
        subHeaderComponent={this.componente}
      />
      </div>
    );
  }
}