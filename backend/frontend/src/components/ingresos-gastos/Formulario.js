import React, { Component } from "react";
import axios from "axios";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Cookies from 'universal-cookie';
const urlAPI = require('../../urlAPI');

export default class Formulario extends Component {
  state = {
    conceptos: [],
    concepto: "",
    monto: "",
    moneda: "",
    date: Date.now(),

    editing: false,

    // Datos financieros del usuario, traidos de la base
    datosFinancieros: [],
    id_datosF: '',
    saldo_ARS: "",
    saldo_USD: "",
    cotizacion_USD: "",
    saldoARS_toUSD: "",
    saldoUSD_toARS: "",

    conceptosIngresos: [],
    conceptosGastos: []
  };

  componentDidMount() {
    const cookie = new Cookies();
    const idUser = cookie.get('user');

    this.getConceptosIngresos(idUser);
    this.getConceptosGastos(idUser);
    this.getDatosFinancieros(idUser);
  }
  
  componentDidUpdate(){

      if (document.getElementById('inputConcept') && document.getElementById('inputMonto') && document.getElementById('inputMoneda') && document.getElementById('inputDate')){
        document.getElementById('inputConcept').style.border = "1px solid gray"
        document.getElementById('inputMonto').style.border = "1px solid gray";
        document.getElementById('inputMoneda').style.border = "1px solid gray";
        document.getElementById('inputDate').style.border = "1px solid gray"; 
      }
  } 

  async getConceptosIngresos(idUser){
    console.log('getingresos')
      const res = await axios.get(urlAPI.getUrl() + "/api/conceptos_ingreso/" + idUser);
      this.setState({ conceptosIngresos: res.data });
  }

  async getConceptosGastos(idUser){
    console.log('getgastos')
      const res = await axios.get(urlAPI.getUrl() + "/api/conceptos_gasto/" + idUser);
      this.setState({ conceptosGastos: res.data });
  }

  async getDatosFinancieros(idUser) {
    const res = await axios.get(urlAPI.getUrl() + "/api/datos_financieros/" + idUser );
    this.setState({ datosFinancieros: res.data });

    if (this.state.datosFinancieros.length > 0){
      this.setState({
        id_datosF: this.state.datosFinancieros[0]._id, 
        saldo_ARS: this.state.datosFinancieros[0].saldo_ARS,
        saldo_USD: this.state.datosFinancieros[0].saldo_USD,
        cotizacion_USD: this.state.datosFinancieros[0].cotizacion_USD,
        saldoARS_toUSD: this.state.datosFinancieros[0].saldoARS_toUSD,
        saldoUSD_toARS: this.state.datosFinancieros[0].saldoUSD_toARS,
      });
    }
  }

  onChangeDate = (date) => {
    const { setDate } = this.props;
    if (this.props.handle.editing) {
      setDate(date);
    } else {
      this.setState({ date: date });
    }
  };

  onInputChange = (e) => {
    this.validatePaste();
    const { stateForm } = this.props;

    if (!this.props.handle.editing) {
      this.setState({
        [e.target.name]: e.target.value,
      });
    } else {
      stateForm(e);
    }
  };

  // Impide pegar texto dentro del input
  validatePaste() {
    const input = document.getElementById("inputMonto");
    input.onpaste = function (e) {
      e.preventDefault();
    };
  }

  validateNumber = (e) => {
    if (this.isNotNumberCode(e)) {
      e.preventDefault(); // Cancela el evento si es una letra
      alert("Debes ingresar un numero");
    }
  };

  isNotNumberCode(e) {
    return !(e.which >= 48 && e.which <= 57) && e.which !== 46;
  }

  onSubmit = async (e) => {
    e.preventDefault();

    // Estados a analizar si son vacios

    const estados = [
      { estado: this.state.concepto, id: "inputConcept" },
      { estado: this.state.monto, id: "inputMonto" },
      { estado: this.state.moneda, id: "inputMoneda" },
      { estado: this.state.date, id: "inputDate" },
    ];

    // Uso del array de estados para analizar cada caso
    if (!this.props.handle.editing) {
      for (let item of estados) {
        this.validateInput(item.estado, item.id);
      }
    }

    // Traigo el estado del componente padre para analzar si se esta editando
    const { handle } = this.props;
    const cookie = new Cookies();
    const cook = cookie.get('user');

    if (this.isFieldsEmpty()) {
      alert("Debes completar todos los campos");
    } else {

      await this.getDatosFinancieros();
      this.actualizarDatosFinancieros(handle, cook);

      if (!handle.editing) {
        console.log('Cotizacion usd: ' + this.state.cotizacion_USD)

        if (this.state.moneda === 'ARS'){
          const newConcepto = {
            idUser: cook,
            concepto: this.state.concepto,
            monto: this.state.monto,
            moneda: this.state.moneda,
            date: this.state.date,
            conversion_dolar: (this.state.monto / (this.state.cotizacion_USD === 0 ? 1 : this.state.cotizacion_USD)).toFixed(2),
            cotizacion_dolar: this.state.cotizacion_USD,
          };
          this.loadConcept(newConcepto);
        } else {
          const newConcepto = {
            idUser: cook,
            concepto: this.state.concepto,
            monto: this.state.monto,
            moneda: this.state.moneda,
            date: this.state.date,
            conversion_dolar: this.state.monto,
            cotizacion_dolar: this.state.cotizacion_USD,
          };
          this.loadConcept(newConcepto);
        }
        this.props.toastAlert('creado');
      } 
      
      else {

        if (handle.moneda === 'ARS'){
          const newConceptoEdit = {
            idUser: handle.idUser,
            concepto: handle.concepto,
            monto: handle.monto,
            moneda: handle.moneda,
            date: handle.date,
            conversion_dolar: (handle.monto / handle.cotizacion_USD).toFixed(2),
            cotizacion_dolar: handle.cotizacion_USD
          };
          this.editConcept(newConceptoEdit, handle.idEdit);
        } else {
          const newConceptoEdit = {
            idUser: handle.idUser,
            concepto: handle.concepto,
            monto: handle.monto,
            moneda: handle.moneda,
            date: handle.date,
            conversion_dolar: handle.monto,
            cotizacion_dolar: handle.cotizacion_USD
          };
          this.editConcept(newConceptoEdit, handle.idEdit);
        }
        this.props.toastAlert('editado');
      }
    }
  };

  actualizarDatosFinancieros(handle, cook) {
      if (handle.editing){
          const montoAEditar = handle.montoEdit;
          const monedaAEditar = handle.monedaEdit;

          const saldoActualARS = monedaAEditar === 'ARS' ? (parseFloat(handle.saldo_ARS) - parseFloat(montoAEditar)) : parseFloat(handle.saldo_ARS);
          const saldoActualUSD = monedaAEditar === 'USD' ? (parseFloat(handle.saldo_USD) - parseFloat(montoAEditar)) : parseFloat(handle.saldo_USD);
          const cotizacionDolar = handle.cotizacion_USD;

          const nuevoSaldoARS = handle.moneda === 'ARS' ? (parseFloat(saldoActualARS) + parseFloat(handle.monto)) : parseFloat(saldoActualARS);
          const nuevoARS_toUSD = 
              handle.moneda === 'ARS' ? (nuevoSaldoARS / cotizacionDolar).toFixed(2) : (saldoActualARS / cotizacionDolar).toFixed(2);

          const nuevoSaldoUSD = 
              handle.moneda === 'USD' ? (parseFloat(saldoActualUSD) + parseFloat(handle.monto)) : saldoActualUSD;
          const nuevoUSD_toARS =
              handle.moneda === 'USD' ? (nuevoSaldoUSD * cotizacionDolar) : (saldoActualUSD * cotizacionDolar);
      
          if (handle.moneda === "ARS") {
            this.setState({
              saldo_USD: saldoActualUSD,
              saldo_ARS: nuevoSaldoARS,
              saldoARS_toUSD: nuevoARS_toUSD
            }, () => {this.updateDatosFinancieros()});
          } else {
            this.setState({
              saldo_ARS: saldoActualARS,
              saldo_USD: nuevoSaldoUSD,
              saldoUSD_toARS: nuevoUSD_toARS
            }, () => {this.updateDatosFinancieros()});
          }
      } 
      
      else {

          const saldoActualARS = this.state.saldo_ARS;
          const saldoActualUSD = this.state.saldo_USD;
          const cotizacionDolar = this.state.cotizacion_USD;

          const route = this.props.prop 

          const nuevoSaldoARS = route === 'ingresos' ? parseFloat(saldoActualARS) + parseFloat(this.state.monto) : parseFloat(saldoActualARS) - parseFloat(this.state.monto);
          const nuevoARS_toUSD = (nuevoSaldoARS / (cotizacionDolar === 0 ? 1 : cotizacionDolar)).toFixed(2);

          const nuevoSaldoUSD = route === 'ingresos' ? parseFloat(saldoActualUSD) + parseFloat(this.state.monto) : parseFloat(saldoActualUSD) - parseFloat(this.state.monto);
          const nuevoUSD_toARS = (nuevoSaldoUSD * (cotizacionDolar === 0 ? 1 : cotizacionDolar ));

          if (this.state.moneda === "ARS") {
            this.setState({
              saldo_ARS: nuevoSaldoARS,
              saldoARS_toUSD: nuevoARS_toUSD,
            }, () => {this.updateDatosFinancieros(cook)});

          } else {
            this.setState({
              saldo_USD: nuevoSaldoUSD,
              saldoUSD_toARS: nuevoUSD_toARS
            }, () => {this.updateDatosFinancieros(cook)});
          }
        }
  }

  async updateDatosFinancieros(cook){
    
    const saldo_ARS = this.state.saldo_ARS;
    const saldo_USD = this.state.saldo_USD;
    const cotizacion_USD = this.state.cotizacion_USD;
    const saldoARS_toUSD = this.state.saldoARS_toUSD;
    const saldoUSD_toARS = this.state.saldoUSD_toARS;

    const datosEdit = {
      id_usuario: cook,
      saldo_ARS,
      saldo_USD, 
      cotizacion_USD,
      saldoARS_toUSD, 
      saldoUSD_toARS
    }

    console.log(datosEdit)

    await axios.put(urlAPI.getUrl() + "/api/datos_financieros/" + cook, datosEdit);
  }

  async editConcept(newConcepto, id) {
    const { reverseStatus } = this.props;

    if (this.props.prop === "ingresos") {
      await axios.put(urlAPI.getUrl() + "/api/ingresos/" + id, newConcepto);
      this.setState({ concepto: "", monto: "", moneda: "" });
      reverseStatus();
    } else {
      await axios.put(urlAPI.getUrl() + "/api/gastos/" + id, newConcepto);
      this.setState({ concepto: "", monto: "", moneda: "" });
      reverseStatus();
    }
  }

  async loadConcept(newConcepto) {
    console.log(newConcepto)
    if (this.props.prop === "ingresos") {
      await axios.post(urlAPI.getUrl() + "/api/ingresos/", newConcepto);
      this.setState({ concepto: "", monto: "", moneda: "" });
    } else {
      await axios.post(urlAPI.getUrl() + "/api/gastos/", newConcepto);
      this.setState({ concepto: "", monto: "", moneda: "" });
    }
  }

  isFieldsEmpty() {
    /* Valida si alguno de los input del Form son vacios */

    if (!this.props.handle.editing) {
      return (
        this.state.concepto === "" ||
        this.state.monto === "" ||
        this.state.moneda === "" ||
        this.state.date === ""
      );
    } else {
      return (
        this.props.handle.concepto === "" ||
        this.props.handle.monto === "" ||
        this.props.handle.moneda === "" ||
        this.props.handle.date === ""
      );
    }
  }

  // Analisis de input, si son vacios, les pone el borde en rojo
  validateInput(estado, id) {
    if (estado === "") {
      document.getElementById(id).style.border = "1px solid red";
      //this.setState({[id]: true}, () => console.log(this.state.inputConcept))
    } else {
      document.getElementById(id).style.border = "1px solid gray";
    }
  }

  render() {
    const titulo = this.props.prop;

    return (
      <div className="row justify-content-center">
        <div className="col-md-8 col-lg-5">
          <div className="card shadow mb-4 mt-4">
            {/* Card header */}
            <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 className="m-0 font-weight-bold text-primary">
                {"Añadir " + titulo.slice(0, titulo.length - 1)}
              </h6>
            </div>

            {/* Card body */}
            <div className="card-body">
                { (titulo === 'ingresos' ?
                this.state.conceptosIngresos.length > 0 :
                this.state.conceptosGastos.length > 0 ) ? 

              <div className="row justify-content-center">
                <form onSubmit={this.onSubmit}>
                  <div className="row">
                    <div className="col-12">
                      <div className="form-group">
                        <label> Concepto </label>
                        <select
                          //name="conceptosIngresos"
                          //style={this.state.inputConcept ? {border: '1px solid red'} : null}
                          className="form-control"
                          id="inputConcept"
                          name="concepto"
                          onChange={this.onInputChange}
                          value={
                            this.props.handle.editing
                              ? this.props.handle.concepto
                              : this.state.concepto
                          }
                        >
                          <option value=""></option>
                          {this.props.prop === "ingresos" ? 
                          
                          this.state.conceptosIngresos.map((concepto) => (
                            <option
                              value={concepto.concepto}
                              key={concepto._id}
                            >
                              {concepto.concepto}
                            </option>
                          )) 
                          
                          : 

                          this.state.conceptosGastos.map((concepto) => (
                            <option
                              value={concepto.concepto}
                              key={concepto._id}
                            >
                              {concepto.concepto}
                            </option>
                          ))
                          } 
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-6">
                      <div className="form-group">
                        <label> Monto</label>
                        <input
                          className="form-control"
                          placeholder="Monto"
                          name="monto"
                          id="inputMonto"
                          value={
                            this.props.handle.editing
                              ? this.props.handle.monto
                              : this.state.monto
                          }
                          onKeyPress={this.validateNumber}
                          onChange={this.onInputChange}
                        />
                      </div>
                    </div>

                    <div className="col-6">
                      <div className="form-group">
                        <label> Moneda </label>
                        <select
                          className="form-control"
                          name="moneda"
                          id="inputMoneda"
                          value={
                            (this.props.handle.editing
                              ? this.props.handle.moneda
                              : this.state.moneda) || ""
                          }
                          onChange={this.onInputChange}
                        >
                          <option value="" key="1"></option>
                          <option value="ARS" key="2">
                            ARS
                          </option>
                          <option value="USD" key="3">
                            USD
                          </option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12">
                      <div className="form-group">
                        <label> Fecha </label>
                        <br />
                        <DatePicker
                          selected={
                            this.props.handle.editing
                              ? this.props.handle.date
                              : this.state.date
                          }
                          //selected={}
                          id="inputDate"
                          onChange={this.onChangeDate}
                          dateFormat="dd/MM/yyyy"
                          className="form-control picker"
                        />
                      </div>
                    </div>
                  </div>
                  <button type="submit" className="btn btn-primary">
                    Enviar
                  </button>

                </form>
              </div> : <h6>{'No se encuentran conceptos ingresados. Para añadir alguno, dirijase a "Ajustes" -> "Añadir Concepto"'}</h6> }
            </div>
          </div>
        </div>
      </div>
    );
  }
}
