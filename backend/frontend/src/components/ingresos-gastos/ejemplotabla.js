import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import axios from 'axios'
import Cookies from 'universal-cookie';
var moment = require('moment');
const urlAPI = require('../../urlAPI');

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: 'concepto', numeric: false, disablePadding: true, label: 'Concepto' },
  { id: 'monto', numeric: true, disablePadding: false, label: 'Monto' },
  { id: 'moneda', numeric: true, disablePadding: false, label: 'Moneda' },
  { id: 'fecha', numeric: false, disablePadding: false, label: 'Fecha' },
  { id: 'conversion_dolar', numeric: true, disablePadding: false, label: 'Conversion a Dolar' },
  { id: 'cotizacion_dolar', numeric: true, disablePadding: false, label: 'Cotizacion de Dolar' },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
            {/* Espacio en blanco, donde iria el chekbox de select all */}
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  //onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  const { numSelected, selected, route, toast, deleteD, datosF} = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
          {numSelected} selected
        </Typography>
      ) : (
        <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
          {route.toUpperCase()}
        </Typography>
      )}

      {/* Muestra el icono de eliminar en caso de seleccionar un item */}
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete" onClick={() => deleteData(selected, route, toast, deleteD, datosF)}>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ): ''}

    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

// Elimina los conceptos recibidos por el prop de elementos seleccionados
async function deleteData(id, route, toast, datosDelete, datosFinancieros){
  let mounted = true 
  const cookie = new Cookies();
  const cook = cookie.get('user');
  let datosF;

  await axios({method:'GET', url: urlAPI.getUrl() + '/api/datos_financieros/' + cook}).then(res => {
    datosF = res.data  
  })

  if (mounted){
    if (route === 'ingresos'){
      for (let i = 0; i < id.length ; i++){ 
        axios({method:'DELETE',url: urlAPI.getUrl() + '/api/ingresos/' + id[i]}) 
      }
    } else {
      for (let i = 0; i < id.length ; i++){ 
        axios({method:'DELETE',url: urlAPI.getUrl() + '/api/gastos/' + id[i]})
      }
    }
    id.length = 0;
    updateDatos(route, datosDelete, datosF[0]);
    toast('eliminado');
  }

  return () => mounted = false
}  

async function updateDatos(route, datos, df){
  //let id = df.id_datoF;
  let idUser = df.id_usuario;
  let saldoARS = df.saldo_ARS;
  let saldoUSD = df.saldo_USD;
  let cotizacionUSD = df.cotizacion_USD;
  //let saldoARS_toUSD = df.saldoARS_toUSD;
  //let saldoUSD_toARS = df.saldoUSD_toARS;

  // Ejemplo de array: [id,monto,moneda , id,monto,moneda ]
  let totalARS = 0;
  let totalUSD = 0;
  for(let i = 0; i < datos.length; i+=3){
    totalARS += datos[i+2] === 'ARS' ? datos[i+1] : 0
    totalUSD += datos[i+2] === 'USD' ? datos[i+1] : 0
  } 
  console.log(saldoARS)
  console.log(totalARS + ' / ' + totalUSD)
  console.log(df)

  const newData = {
    saldo_ARS: route === 'ingresos' ? saldoARS - totalARS : saldoARS + totalARS 
    , saldo_USD: route === 'ingresos' ? saldoUSD - totalUSD : saldoUSD + totalUSD 
    , saldoARS_toUSD: route === 'ingresos' ? ((saldoARS - totalARS) / (cotizacionUSD === 0 ? 1 : cotizacionUSD)).toFixed(2) : ((saldoARS + totalARS) / (cotizacionUSD === 0 ? 1 : cotizacionUSD)).toFixed(2)
    , saldoUSD_toARS: route === 'ingresos' ? (saldoUSD - totalUSD) * cotizacionUSD : (saldoUSD + totalUSD) * cotizacionUSD //calcular(route, cotizacionUSD, (saldoARS - totalARS), 'USDtoARS')
    , cotizacion_USD: cotizacionUSD
    , id_usuario: idUser
  }

  console.log(newData)

  datos.length = 0; // Vacio el aray de elementos seleccionados

  await axios.put( urlAPI.getUrl() + '/api/datos_financieros/' + idUser, newData)
}

export default function EnhancedTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('fecha');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const [ingresos, setIngresos] = React.useState([]);
  const [selectedBis, setSelectedBis] = React.useState([]);
  //const [datosFinancieros, setDatosFinancieros] = React.useState([]);

  const route = props.route
  const toastAlert = props.toastAlert
  const datosFinancieros = props.datosFinancieros
  //const idUserLogged = props.idUserLogged
  
  // Peticion API - Hooks
  useEffect(() => {
    const cook = new Cookies();
    const idUserLogged = cook.get('user')
    let mounted = true 

    if (mounted){
      if (route === "ingresos"){
        axios({method:'GET', url: urlAPI.getUrl() + '/api/ingresos/' + idUserLogged}).then(res => {
          if (mounted){
            setIngresos(res.data)
          }
        })
      } else {
        axios({method:'GET', url: urlAPI.getUrl() + '/api/gastos/' + idUserLogged}).then(res => {
          if (mounted){
            setIngresos(res.data)
          }
        })
      }
    }

    return () => mounted = false;

  }, [ingresos, selected, route])

  

  function buildData(arrayIngresos){
    const rows = []
    for(let i = 0 ; i < arrayIngresos.length ; i++ ){
      rows[i] = extractData(
        arrayIngresos[i]._id, 
        arrayIngresos[i].concepto, 
        arrayIngresos[i].monto,
        arrayIngresos[i].moneda,
        moment(arrayIngresos[i].date).format("DD/MM/YYYY"),
        arrayIngresos[i].conversion_dolar,
        arrayIngresos[i].cotizacion_dolar,
      )
    }
    return rows
  }

  function extractData(_id, concepto, monto, moneda, date, conversion_dolar, cotizacion_dolar){
    return { _id, concepto, monto, moneda, date, conversion_dolar, cotizacion_dolar } 
  }
  
  // Data a mostrar en la tabla
  const rows = buildData(ingresos)

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  /* const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.concepto); //Mapea por concepto
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };  */

  const handleClick = (event, name, monto, moneda) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);

    // Array para eliminar y manipular datos Financieros del user
    const selectedIndexBis = selectedBis.indexOf(name)
    let editMontos = [];

    if (selectedIndexBis === -1) {
      editMontos = editMontos.concat(selectedBis, name, monto, moneda);
    } else if (selectedIndexBis === 0) {
      editMontos = editMontos.concat(selectedBis.slice(3));
    } else if (selectedIndexBis === selectedBis.length - 1) {
      editMontos = editMontos.concat(selectedBis.slice(0, -1), monto, moneda);
    } else if (selectedIndexBis > 0) {
      editMontos = editMontos.concat(
        selectedBis.slice(0, selectedIndexBis),
        selectedBis.slice(selectedIndexBis + 3),
      );
    } 

    setSelectedBis(editMontos);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  /* const handleChangeDense = (event) => {
    setDense(event.target.checked);
  }; */

  const isSelected = (name) => selected.indexOf(name) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  return (
    <div className="container-fluid">

    <div className={classes.root}>
      <Paper className={classes.paper}>
        <EnhancedTableToolbar numSelected={selected.length} selected={selected} route={route} toast={toastAlert} deleteD={selectedBis} datosF={datosFinancieros}/>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            //size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              //onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            /> 
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row._id);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleClick(event, row._id, row.monto, row.moneda)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row._id}
                      selected={isItemSelected} 
                    > 
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                        /> 
                      </TableCell>
                      <TableCell component="th" id={labelId} scope="row" padding="none">
                        {row.concepto}
                      </TableCell> 

                      {/* Agregar datos */}
                      <TableCell align="left">{row.monto}</TableCell>
                      <TableCell align="left">{row.moneda}</TableCell>
                      <TableCell align="left">{row.date}</TableCell>
                      <TableCell align="left">{'$ ' + row.conversion_dolar}</TableCell>
                      <TableCell align="left">{'$ ' + row.cotizacion_dolar}</TableCell>

                      {/*concepto, monto, moneda, date, conversion_dolar, cotizacion_dolar */}
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
    </div>
  );
}
