import React, { Component } from "react";

// Componentes
//import Tabla from './ingresos-gastos/Tabla'; 
import Formulario from './ingresos-gastos/Formulario'
import moment from 'moment';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Ejemplotabla from './ingresos-gastos/ejemplotabla'
import Sidebar from './Sidebar'
import Cookies from 'universal-cookie'

const cookie = new Cookies();
const cook = cookie.get('user')
const urlAPI = require('../urlAPI');

export default class Ingresos extends Component {
  // Estado que comunica a los componentes Tabla y Formulario
  state = {

    //Usuario harcodeado
    idUser: cook,
    //idUser: this.props.idUserLogged,

    editing: false,
    idEdit: '',
    itemEdit: '',

    montoEdit: '',
    monedaEdit: '',
    //Estados para el form
    concepto: '',
    monto: '',
    moneda: '',
    date: '',

    // Datos financieros
    datosFinancieros: [],
    id_datoF: '',
    saldo_ARS: '',
    saldo_USD: '',
    cotizacion_USD: '',
    saldoARS_toUSD: '',
    saldoUSD_toARS: '',
  }

  componentDidMount(){
    this.getDatosFinancieros();
    console.log(this.props)
  }

  async getDatosFinancieros() {
    const cookie = new Cookies();
    const cook = cookie.get('user')
    const res = await axios.get(urlAPI.getUrl() + "/api/datos_financieros/" + cook);
    this.setState({ datosFinancieros: res.data }, () => console.log(res.data));

    this.setState({
      id_datoF: res.data[0]._id, 
      saldo_ARS: res.data[0].saldo_ARS,
      saldo_USD: res.data[0].saldo_USD,
      cotizacion_USD: res.data[0].cotizacion_USD,
      saldoARS_toUSD: res.data[0].saldoARS_toUSD,
      saldoUSD_toARS: res.data[0].saldoUSD_toARS,
    }); 
  }

  handleChange = (item,texto) => {
    this.setState({
      editing: true,
      idEdit: item._id, // Trae el ID desde el hijo
      itemEdit: item,
      montoEdit: item.monto,
      monedaEdit: item.moneda
    });
    this.loadItemInForm(item);
    this.toastAlert(texto);
  }

  // Toasts
  toastAlert(texto){
    toast.info('Concepto ' + texto, {
      position: "top-right",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      });
  }

  loadItemInForm(item){
    const fechaFormateada = moment(item.date).toDate()
    this.setState(
      {
        concepto: item.concepto,
        monto: item.monto,
        moneda: item.moneda,
        date: fechaFormateada
      }
    );
  }

  setDate = date => {
    this.setState({
      date: date
    });
  }

  setStateForm = e =>{
    this.setState({
      [e.target.name] : e.target.value
    });
  }

  reverseStatus = e => {
    this.setState({
      editing: false,
      idEdit: '',
      montoEdit: '',
      monedaEdit: ''
    });
    this.getDatosFinancieros();
  }

  cancelEdition = e => {
    this.setState({
      editing: false
    });
    this.toastAlert('cancelado')
  }

  render() {
    // Obtengo el nombre de la ruta
    const props = this.props.route

    return (
      <div>
        <Sidebar
          route = {props}

          componente={[
            <Formulario
              prop={props}
              handle={this.state}
              reverseStatus={this.reverseStatus}
              stateForm={this.setStateForm}
              setDate={this.setDate}
              cancelEdition={this.cancelEdition}
              toastAlert={this.toastAlert}
              key={'1'}
              idUserLogged={this.props.idUserLogged}

              refreshDf={this.getDatosFinancieros}
            />,

            <Ejemplotabla 
              route={props} 
              key={'3'}
              toastAlert={this.toastAlert}  
              datosFinancieros={this.state} 
              idUserLogged={this.props.idUserLogged}           
            />,

            <ToastContainer
              position="top-right"
              autoClose={1000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              limit={3}
              key={'2'}
            />,
          ]}
        />
      </div>
    );
  }
}
