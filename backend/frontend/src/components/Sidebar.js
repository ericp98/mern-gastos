import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import {RiMoneyDollarCircleLine} from 'react-icons/ri'
import {GiPayMoney, GiReceiveMoney} from 'react-icons/gi'
import {IoIosSettings} from 'react-icons/io'
import {AiOutlineUser} from 'react-icons/ai'
import {RiLogoutBoxLine} from 'react-icons/ri'
import Cookies from 'universal-cookie';

export default class Sidebar extends Component {
    
    active(routeName){
        const route = this.props.route
        if (route === routeName){
            return 'active'
        }
    }

    deleteCookie(){
        const cookie = new Cookies();
        cookie.remove('user')
        window.location.href='./'
    }

    render() {
        return (
            <div id="wrapper">
                <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar"> 

                    {/* <!-- Sidebar - Brand --> */}
                    <Link to={'inicio'} className="sidebar-brand d-flex align-items-center justify-content-center" >
                        <div className="sidebar-brand-icon rotate-n-15">
                            <i className="fas fa-laugh-wink"></i>
                        </div>
                        <div className="sidebar-brand-text mx-3">EarnAdmin </div>
                    </Link>

                    {/* <!-- Divider --> */}
                    <hr className="sidebar-divider my-0" />

                    {/* <!-- Nav Item - Dashboard --> */}
                    <li className={"nav-item " + this.active('inicio')}>
                        <Link to="/inicio" className="nav-link">
                            <RiMoneyDollarCircleLine size={"1.5em"}/>
                            <span> Inicio</span>
                        </Link>
                    </li>

                    {/* <!-- Divider --> */}
                    <hr className="sidebar-divider" />

                    {/* <!-- Heading --> */}
                    <div className="sidebar-heading">
                        Administracion
                    </div>

                    {/* <!-- Nav Item - Pages Collapse Menu --> */}
                    <li className={"nav-item " + this.active('ingresos')}>
                        <Link to="/ingresos" className="nav-link collapsed" aria-expanded="true">
                            <GiReceiveMoney size={"1.5em"}/>
                            <span>  Ingresos</span>
                        </Link>
                    </li>

                    {/* <!-- Nav Item - Utilities Collapse Menu --> */}
                    <li className={"nav-item " + this.active('gastos')}>
                        <Link to="/gastos" className="nav-link collapsed" aria-expanded="true">                            
                            <GiPayMoney size={"1.5em"} />
                            <span>  Gastos</span>
                        </Link>
                    </li>

                    {/* <!-- Divider --> */}
                    <hr className="sidebar-divider" />

                    {/* <!-- Heading --> */}
                    <div className="sidebar-heading">
                        CONFIGURACION
                    </div>

                    {/* <!-- Nav Item - Pages Collapse Menu --> */}
                        <li className={"nav-item " + this.active('settings')}>
                            <Link to="/settings" className="nav-link collapsed" aria-expanded="true">
                            <IoIosSettings size={"1.5em"}/>
                            <span>  Ajustes</span>
                            </Link>
                        </li>

                    {/* <!-- Nav Item - Charts --> */}
                    <li className={"nav-item " + this.active('usuario')} hidden>
                        <Link to="/inicio" className="nav-link">
                            <AiOutlineUser size={"1.5em"} />
                            <span>  Usuario</span>
                        </Link>
                    </li>

                    {/* <!-- Divider --> */}
                    <hr className="sidebar-divider d-none d-md-block" />

                    <li className={"nav-item " + this.active('logOut')}>
                        {/* <button onClick={() => this.deleteCookie()}>LOGOut</button> */}
                        <Link onClick={() => this.deleteCookie()} to='/' className="nav-link"> 
                            <RiLogoutBoxLine size={"1.5em"}/>
                            <span>  Cerrar Sesión</span>
                        </Link>
                    </li>

                </ul> 

                <div id="content-wrapper" className="d-flex flex-column">
                    {this.props.componente}
                </div>
            </div>
        )
    }
}
