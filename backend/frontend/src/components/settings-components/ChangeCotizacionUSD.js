import React, { Component } from 'react';
import Cookies from 'universal-cookie';
import {RiMoneyDollarCircleLine} from 'react-icons/ri';
import Axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
const urlAPI = require('../../urlAPI');

export default class ChangeCotizacionUSD extends Component {

    state = {
        newCotizacion: '',
        cotizacion_USD: '',
    }

    componentDidMount(){
        const cookie = new Cookies();
        const idUser = cookie.get('user');

        this.getDatosFinancieros(idUser);
    }

    async getDatosFinancieros(idUser){
        const res = await Axios.get(urlAPI.getUrl() + '/api/datos_financieros/' + idUser);
        this.setState({cotizacion_USD: res.data[0].cotizacion_USD}, () => console.log(this.state.cotizacion_USD));
    }

    onChange = e => {
        this.setState({newCotizacion: e.target.value}, () => console.log(this.state.newCotizacion))
    }

    async setCotizacion(){
        const cookie = new Cookies();
        const idUser = cookie.get('user');
        const cotizacion_USD = this.state.newCotizacion;

        if (cotizacion_USD === ''){
            return alert('Se debe ingresar un valor valido');
        }

        await Axios.patch(urlAPI.getUrl() + '/api/datos_financieros/' + idUser, {cotizacion_USD});

        this.setState({newCotizacion: ''});
        await this.getDatosFinancieros(idUser);

        this.toastAlert('Cotizacion modificada');
    }

    toastAlert(texto){
        toast.info(texto, {
            position: "top-right",
            autoClose: 1500,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }

    render() {

        return (
            <div className="row justify-content-center">   
                    
                <div className="col-md-8 col-lg-5 " >

                    {/* Pie chart */}
                    <div className={"card shadow mb-4 mt-4"}>

                        {/* Card header */}
                        <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 className="m-0 font-weight-bold text-primary"><div> <RiMoneyDollarCircleLine size='1.5em'/> Cambiar cotizacion USD </div></h6>
                        </div>

                            {/* Card body */}
                            <div className="card-body">
                                <div className="row justify-content-left d-flex flex-row "> 
                                    Cotizacion actual: {'$ ' + this.state.cotizacion_USD }                                    
                                    <input 
                                        style={{marginTop: '1em'}}
                                        className="form-control"
                                        type="number" 
                                        placeholder='Nueva cotizacion'
                                        onChange={this.onChange}
                                        value={this.state.newCotizacion}
                                        //id='conceptoInput'
                                    />                           
                                </div>                                                                                                 
                            </div>

                            <div className="card-footer">
                                <div className="justify-content-end d-flex flex-row ">
                                    <button className="btn btn-primary" onClick={() => this.setCotizacion()}>Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <ToastContainer
                        position="top-right"
                        autoClose={1000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                        limit={3}
                        key={'2'}
                    />
                          
            </div>
        )
    }
}
