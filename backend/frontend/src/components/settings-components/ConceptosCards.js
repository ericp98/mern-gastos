import React, { Component } from 'react';
import Axios from 'axios';
import IngresoConcepto from './IngresoConcepto';
import {GiPayMoney, GiReceiveMoney} from 'react-icons/gi';
const urlAPI = require('../../urlAPI');

export default class Card extends Component {

    async removeItemFromArray(arr, concepto, id_concepto){
        let element = document.getElementById(id_concepto);
        let ruta = element.getAttribute('concepto');

        for (let i = 0 ; arr.length ; i++){
            let element = arr[i].concepto.indexOf(concepto);

            if (element !== -1){
                arr.splice(i, 1);
                break;
            }
        }

        if (ruta === "Gastos"){
            await Axios.delete(urlAPI.getUrl() + '/api/conceptos_gasto/' + id_concepto);
        } else {
            await Axios.delete(urlAPI.getUrl() + '/api/conceptos_ingreso/' + id_concepto);
        }

        element.style.display = "none"; // Saca el elemento de la pantalla

        this.refreshConceptos()
    }

    isElementInArray(arr, item){
        var i = arr.indexOf(item);
        return i === -1
    }

    refreshConceptos = e => {
        const refresh = this.props.refresh;
        refresh();
    }

    render() {
        const titles = ['Ingresos', 'Gastos'];
        const data = this.props.data;
        console.log(data)
        
        return (
            <div> 
                <div className="row justify-content-center">   
                    {data.map((item, index) => 
                            <div className="col-md-8 col-lg-5 " key={index} >

                                {/* Pie chart */}
                                <div className={"card shadow mb-4 mt-4"}>

                                    {/* Card header */}
                                    <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 className="m-0 font-weight-bold text-primary">
                                            {titles[index] === 'Ingresos' ? 
                                                <div><GiReceiveMoney size={'1.5em'}/> Conceptos de Ingresos </div>
                                                : 
                                                <div><GiPayMoney size={'1.5em'}/> Conceptos de Gastos</div>}
                                        </h6>
                                    </div>

                                        {/* Card body */}
                                        <div className="card-body">
                                            <div className="row justify-content-left d-flex flex-row ">
                                            { item.length !== 0 ? // Si existe algun concepto cargado
                                            item.map((concepto) => 
                                                
                                                <div className="d-flex flex-row justify-content-between" key={concepto._id}> 
                                                    <div className="badge badge-pill badge-dark mr-3 mb-3"
                                                        id={concepto._id} 
                                                        key={concepto._id} 
                                                        concepto={titles[index]}
                                                        onClick={() => this.removeItemFromArray(item, concepto.concepto, concepto._id)} 
                                                    >
                                                        <h6 className='my-auto'>{concepto.concepto}</h6>                                                           
                                                    </div> 
                                                </div>                                                 
                                            ): <div className='mx-auto'> {'No hay conceptos de ' + titles[index].toLowerCase() + ' cargados'} </div> }  
                                        </div>                                                                                    
                                    </div>
                                    <div className="card-footer">
                                            <div className="justify-content-end d-flex flex-row ">
                                                <IngresoConcepto 
                                                    concepto={titles[index]}
                                                    refresh={this.refreshConceptos}    
                                                />
                                            </div>
                                    </div>
                                </div>
                            </div>
                        )}     
                    </div>
                </div>
        )
    }
}
