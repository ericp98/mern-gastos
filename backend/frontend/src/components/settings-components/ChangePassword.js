import React, { Component } from 'react';
import Cookies from 'universal-cookie';
import { ToastContainer, toast } from 'react-toastify';
import {IoIosSettings} from 'react-icons/io';
import axios from 'axios';
const urlAPI = require('../../urlAPI');

export default class ChangePassword extends Component {

    state = {
        oldPassword: '',
        newPassword: ''
    }

    onChange = e => {
        this.setState({oldPassword: e.target.value});
    }

    onChangePassword = e => {
        this.setState({newPassword: e.target.value});
    }

    async changePass(){
        const cookies = new Cookies();
        const idUser = cookies.get('user');
        const oldPassword = this.state.oldPassword;
        const newPassword = this.state.newPassword;

        const newPass = {
            id: idUser,
            oldPassword: oldPassword,
            newPassword: newPassword,
        }

        const res = await axios.patch(urlAPI.getUrl() + '/changePassword', newPass);
        
        if (res.data.isValid){
            this.setState({newPassword: '', oldPassword: ''});
            this.toastAlert('Contraseña modificada')
        } else {
            this.setState({newPassword: '', oldPassword: ''});
            this.toastAlert('Contraseña erronea', 'error')
        }
    } 

    toastAlert(texto, type){
        if (type !== 'error'){
            toast.info(texto, {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } else {
            toast.error(texto, {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    }

    render() {
        return (
            <div className="row justify-content-center">   
                    
                <div className="col-md-8 col-lg-5 " >

                    {/* Pie chart */}
                    <div className={"card shadow mb-4 mt-4"}>

                        {/* Card header */}
                        <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 className="m-0 font-weight-bold text-primary"> <IoIosSettings size='1.5em'/> Cambiar contraseña</h6>
                        </div>

                            {/* Card body */}
                            <div className="card-body">
                                <div className="row justify-content-left d-flex flex-row ">                                        
                                    <input 
                                        className="form-control"
                                        type="password" 
                                        placeholder='Contraseña actual'
                                        value={this.state.oldPassword}
                                        onChange={this.onChange}
                                        //id='conceptoInput'
                                    />    

                                    <input 
                                        style={{marginTop: '1em'}}
                                        className="form-control"
                                        type="password" 
                                        placeholder='Contraseña nueva'
                                        value={this.state.newPassword}
                                        onChange={this.onChangePassword}
                                        //id='conceptoInput'
                                    />                           
                                </div>                                                                                                 
                            </div>

                            <div className="card-footer">
                                <div className="justify-content-end d-flex flex-row ">
                                    <button className="btn btn-primary" onClick={() => this.changePass()}>Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <ToastContainer
                        position="top-right"
                        autoClose={1000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                        limit={3}
                        key={'2'}
                    />
                          
            </div>
        )
    }
}
