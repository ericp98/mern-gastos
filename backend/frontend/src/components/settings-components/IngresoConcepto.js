import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ToastContainer, toast } from 'react-toastify';
import Axios from 'axios';
import Cookies from 'universal-cookie';
const urlAPI = require('../../urlAPI');

export default class IngresoConcepto extends Component {

    state = {
        showModal: false
    }

    handleShow(bool){
        this.setState({showModal: bool})
    }

    sentData(){
        let input = (document.getElementById("conceptoInput").value).trim(); // Obtengo input del form del modal
        if (input === ''){
            this.toastAlert('Debes ingresar un concepto valido', 'error')
        } else {
            this.sentConcepto(input);
            this.toastAlert('Concepto añadido')
        }  
        console.log(input)
        this.handleShow(false, 'error');
    }

    async sentConcepto(input){
        const refresh = this.props.refresh;
        const cookie = new Cookies();
        const idUser = cookie.get('user'); 

        const newConcepto = {
            id_usuario: idUser,
            concepto: input
        }

        console.log(this.props.concepto);

        if (this.props.concepto === 'Ingresos'){
            await Axios.post(urlAPI.getUrl() + '/api/conceptos_ingreso/', newConcepto);
        } else {
            await Axios.post(urlAPI.getUrl() + '/api/conceptos_gasto/', newConcepto);
        } 

        refresh();
        
    }

    toastAlert(texto, type){
        if (type === 'error'){
            toast.error(texto, {
                position: "top-right",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } else {
            toast.info(texto, {
                position: "top-right",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }

      }

    render() {
        return (
            <div>
                <button 
                    className="btn btn-primary"
                    onClick={() => this.handleShow(true)}    
                > 
                    Añadir 
                </button>

                <Modal show={this.state.showModal} onHide={() => this.handleShow(false)} >

                    <Modal.Header closeButton >
                        <Modal.Title > Ingresar nuevo concepto </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div>
                            <input 
                                className="form-control"
                                type="text" 
                                placeholder='Concepto'
                                id='conceptoInput'
                            />
                        </div>
                    </Modal.Body>

                    <Modal.Footer>
                        <button onClick={() => this.sentData()} className='btn btn-primary'> Añadir </button>
                        <button onClick={() => this.handleShow(false)} className='btn btn-danger'> Cancelar </button>                      
                    </Modal.Footer>

                </Modal>

                <ToastContainer
                    position="top-right"
                    autoClose={1000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    limit={3}
                    key={'2'}
                />
            </div>
        )
    }
}
