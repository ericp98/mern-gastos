import React, { Component } from 'react';
import axios from 'axios';
import Cookies from 'universal-cookie';
import {Link} from 'react-router-dom';
import Alert from 'react-bootstrap/Alert';
const urlAPI = require('../urlAPI');

export default class FormularioUsers extends Component {

    state = {
        route: this.props.route,
        email: '',
        password: '',
        userID: '',
        hiddeMessageError: true,
    }

    setUser(value){
        this.setState({email: value})
    }

    setPass(value){
        this.setState({password: value})
    }

    sendRequest = async (event) => {
        event.preventDefault();

        if (this.props.route === 'login'){
            const res = await axios.post(urlAPI.getUrl() + '/signin', {email: this.state.email, password:this.state.password})
            this.setState({userID: res.data.userID})
            this.validateRequest(res.data.isUserValid, res.data.userID)   
        } else {
            const res = await axios.post(urlAPI.getUrl() + '/signup', {email: this.state.email, password: this.state.password});
            this.validateRequest(res.data.isUserValid, res.data.idNewUser);
        }
    }

    async validateRequest(isAuthenticate, idUser){
        // Setea cookie de login en App.js
        const { handleLoggin } = this.props

        if (this.props.route === 'login'){
            if(isAuthenticate){
                const cookies = new Cookies();
                cookies.set('user', this.state.userID, {path: '/'});
                handleLoggin(cookies);
            } else {
                this.setState({hiddeMessageError: false, password:''});
            }
        } else {
            if (isAuthenticate){
                await axios.post(urlAPI.getUrl() + "/api/datos_financieros",{id_usuario:idUser ,"saldo_ARS":0,"saldo_USD":0,"cotizacion_USD":0,"saldoARS_toUSD":0,"saldoUSD_toARS":0});
                alert('Usuario registrado exitosamente');
                window.location.href='./';
            } else {
                this.setState({password: '', hiddeMessageError: false})
            }
        }
    }

    render() {

        const route = this.props.route;
        document.body.classList.add('bg-gradient-primary');

        return (
            <div className="container">

                <Alert 
                    variant="danger" 
                    onClose={() => this.setState({hiddeMessageError: true}) }
                    dismissible
                    show={!this.state.hiddeMessageError}
                >
                    {route === 'login' ? 'Usuario y/o contraseña incorrectos' : 'Usuario ya registrado. Ingrese uno diferente'}
                </Alert>

              <div className="row justify-content-center">
                <div className="col-xl-10 col-lg-12 col-md-9">
                  <div className="card o-hidden border-0 shadow-lg my-5">
                    <div className="card-body p-0">
                      <div className="row">
                        <div className="col-lg-3 d-none d-lg-block "></div>
                        <div className="col-lg-6">
                          <div className="p-5">
                            <div className="text-center">
                              <h1 className="h4 text-gray-900 mb-4">
                                { route === 'login' ? 'Iniciar Sesión' : 'Registarse'}
                              </h1>
                            </div>
                            <form className="user">
                              <div className="form-group">
                                <input
                                  className="form-control form-control-user"
                                  id="exampleInputEmail"                                                                
                                  type="text" 
                                  name="email" 
                                  placeholder={ route === 'login' ? "Usuario" : "Nombre de usuario"}
                                  onChange={(e) => this.setUser(e.target.value)}
                                />
                              </div>
                              <div className="form-group">
                                <input
                                  className="form-control form-control-user"
                                  id="exampleInputPassword"
                                  type="password" 
                                  name="password" 
                                  placeholder="Contraseña"
                                  onChange={(e) => this.setPass(e.target.value)}
                                  value={this.state.password}
                                />
                              </div>
                              <button 
                                onClick={this.sendRequest} 
                                className="btn btn-primary btn-user btn-block">
                                    { route === 'login' ? 'Ingresar' : 'Registrarse'}
                              </button>
                            </form>
                            <hr /> 
                            <div className="text-center">
                              {route === 'login' ?
                                <Link className="small" to='/register'> 
                                    Crear una cuenta
                                </Link> 
                                
                                :

                                <Link className="small" to='/'> 
                                    Volver al inicio
                                </Link>
                                // Verificar registro
                              }
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        );
    }
}
