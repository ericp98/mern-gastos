import React, { Component } from 'react';
import Cookies from 'universal-cookie';
import Axios from 'axios';

// Components
import Card from './settings-components/ConceptosCards';
import Sidebar from './Sidebar';
import ChangePassword from './settings-components/ChangePassword';
import ChangeCotizacionUSD from './settings-components/ChangeCotizacionUSD';

const urlAPI =  require('../urlAPI');

export default class Settings extends Component {

    state = {
        conceptosIngresos: [],
        conceptosGastos: []
    }

    componentDidMount(){
        const cookies = new Cookies();
        const idUser = cookies.get('user');

        this.getConceptosIngresos(idUser);
        this.getConceptosGastos(idUser);
    }

    refreshConceptos = e =>{
        const cookies = new Cookies();
        const idUser = cookies.get('user');

        this.getConceptosIngresos(idUser);
        this.getConceptosGastos(idUser); 
    }

    consol(){
        console.log('console')
    }

    async getConceptosIngresos(idUser){
        const res = await Axios.get(urlAPI.getUrl() + '/api/conceptos_ingreso/' + idUser);
        this.setState({conceptosIngresos: res.data});
    }

    async getConceptosGastos(idUser){
        const res = await Axios.get(urlAPI.getUrl() + '/api/conceptos_gasto/' + idUser);
        this.setState({conceptosGastos: res.data});
    }

    render() {

        document.body.classList.add('bg-gradient-primary');

        return (
            <div>
                <Sidebar 
                
                    route={'settings'}
                    
                    componente={[
                        <Card 
                            data={[
                                this.state.conceptosIngresos,
                                this.state.conceptosGastos
                            ]}
                            key={'1'}
                            refresh={this.refreshConceptos}
                        />,
                        <ChangeCotizacionUSD />,
                        <ChangePassword />
                    ]} 
                />
                
            </div>
        )
    }
}
