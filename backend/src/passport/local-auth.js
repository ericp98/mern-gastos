const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/UserModel');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser( async (id, done) => {
    const user = await User.findById(id);
    done(null, user);
});

passport.use('local-signup', new LocalStrategy({
    usernameField: 'email', // nombre en el form
    passwordField: 'password', // nombre en form
    passReqToCallback: true
}, async (req, email, password, done) => {

    const user = await User.findOne({email: email});
    if (user){
        return done(null, false, req.flash('signupMessage', 'The Email is already taken'))
    } else {
        const newUser = new User();
        newUser.email = email;
        newUser.password = await newUser.encryptPassword(password);
        await newUser.save();
        done(null, newUser);
    }
})); 

passport.use('local-signin',  new LocalStrategy({
    usernameField: 'email', // nombre en el form
    passwordField: 'password', // nombre en form
    passReqToCallback: true
}, async (req, email, password, done) => {
    const user = await User.findOne({email: email});
    if (!user){
        done(null, false, req.flash('signinMessage', 'No user found'))
    } else {
        const match = await user.matchPassword(password);
        if (match){
            //console.log(user)
            return done(null, user)
        } else {
            return done(null, false, req.flash('signinMessage', 'No user found'))
        }
    }
}))   