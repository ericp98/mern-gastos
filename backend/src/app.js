const express = require('express');
const engine = require('ejs-mate');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const flash = require('connect-flash');
const session = require('express-session');
const cookieParser = require("cookie-parser");
const passport = require('passport');
const morgan = require('morgan');

// Initializacions
require('./passport/local-auth.js');

//Settings 
//app.set('views', path.join(__dirname, 'views'));
//app.engine('ejs', engine);
//app.set('view engine', 'ejs');
app.set('port', process.env.PORT || 4000)

//Middlewars
app.use(express.static(path.resolve(__dirname, '../frontend/build')));
app.use(morgan('dev'));
app.use(express.urlencoded({extended:true})) // Indica datos que se reciben del cliente
app.use(cors('*')); // Link react component
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

//////// Configurar sesion
app.use(session({
    secret: 'mysecretsession',
    resave: false, 
    saveUninitialized: false
}));
app.use(cookieParser("mysecretsession"));
/////////////////////////////

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
    app.locals.signupMessage = req.flash('signupMessage');
    app.locals.signinMessage = req.flash('signinMessage');
    next();
});

//Routes
app.use('/', require('./routes/users.js'));
//app.use('/api/users', require('./routes/users'));
app.use('/api/ingresos', require('./routes/ingresos.js'));
app.use('/api/gastos', require('./routes/gastos.js'));
app.use('/api/datos_financieros', require('./routes/datosFinancieros.js'));
app.use('/api/conceptos_ingreso', require('./routes/coneptosIngreso.js'));
app.use('/api/conceptos_gasto', require('./routes/conceptosGasto.js'));

app.get('*', function(request, response) {
    response.sendFile(path.resolve(__dirname, '../frontend/build', 'index.html'));
});

module.exports = app;