const conceptoGastosCtrl = {};

const ConceptoG = require('../models/ConceptosGasto');

conceptoGastosCtrl.getConceptos = async (req, res) => {
    const conceptos = await ConceptoG.find();
    res.json(conceptos);
}

conceptoGastosCtrl.getConceptosByUser = async (req, res) => {
    const conceptos = await ConceptoG.find({id_usuario: req.params.id});
    res.json(conceptos);
};

conceptoGastosCtrl.createConcepto = async (req, res) => {
    const { concepto, id_usuario } = req.body;
    const newConcepto = new ConceptoG ({
        id_usuario,
        concepto: concepto
    });
    await newConcepto.save();
    res.json({message: "Concepto agregado"});
}

conceptoGastosCtrl.deleteConcepto = async (req, res) => {
    await ConceptoG.findOneAndDelete({_id: req.params.id});
    res.json({message: "Concepto deleted"});
}

module.exports = conceptoGastosCtrl;