const mongoose = require('mongoose');
const castAggregation = require('mongoose-cast-aggregation');
mongoose.plugin(castAggregation); 

const datosFinancierosctrl = {};

const DatosF = require('../models/DatosFinancieros');

datosFinancierosctrl.getDatosF = async (req, res) => {
    const datos = await DatosF.find();
    res.json(datos);
};

datosFinancierosctrl.getById = async (req, res) => {
    const datos = await DatosF.find({id_usuario: req.params.id});
    res.json(datos);
};

datosFinancierosctrl.createDatoF = async (req, res) => {
    const { id_usuario, saldo_ARS, saldo_USD, cotizacion_USD, saldoARS_toUSD, saldoUSD_toARS } = req.body;
    const newDatoF = new DatosF({
        id_usuario: id_usuario,
        saldo_ARS: saldo_ARS,
        saldo_USD: saldo_USD,
        cotizacion_USD: cotizacion_USD,
        saldoARS_toUSD: saldoARS_toUSD,
        saldoUSD_toARS: saldoUSD_toARS
    });
    await newDatoF.save();
    res.json({message: "Dato creado"});
};

datosFinancierosctrl.editDato = async (req, res) => {
    const { id_usuario,saldo_ARS, saldo_USD, cotizacion_USD, saldoARS_toUSD, saldoUSD_toARS } = req.body;
    await DatosF.findOneAndUpdate({id_usuario: req.params.id}, {
        id_usuario,
        saldo_ARS,
        saldo_USD,
        cotizacion_USD,
        saldoARS_toUSD,
        saldoUSD_toARS
    });
    res.json({message: "dato editado"})
}; 

datosFinancierosctrl.deleteDato = async (req, res) => {
    await DatosF.findOneAndDelete(req.params.id);
    res.json({message: "Dato elimindao"});
};
 
datosFinancierosctrl.groupDatosF = async (req, res) => {
    const resp = await DatosF.aggregate([
        {
            $match: {
                id_usuario: req.params.idUser
            }
        },
    ]);
    res.json(resp);
}   

datosFinancierosctrl.editSomeDato = async (req,res) => {
    const { cotizacion_USD } = req.body
    await DatosF.findOneAndUpdate({id_usuario: req.params.id}, {cotizacion_USD});
    res.json('dato updated');
}

module.exports = datosFinancierosctrl;