const ingresosCtrl = {};

const Ingreso = require('../models/Ingresos');

ingresosCtrl.getIngresos = async (req,res) => {
    const ingresos = await Ingreso.find();
    res.json(ingresos);
};

ingresosCtrl.getIngresosUser = async (req,res) => {
    const resp = await Ingreso.aggregate([
        {
            $match: {
                idUser: req.params.id
            }
        }
    ]);
    res.json(resp);
} 

ingresosCtrl.createIngreso = async (req, res) => {
    const { idUser, title, concepto, monto, moneda, date, conversion_dolar, cotizacion_dolar } = req.body;
    const newIngreso = new Ingreso({
        idUser,
        title,
        concepto,
        monto,
        moneda,
        date,
        conversion_dolar,
        cotizacion_dolar
    });
    await newIngreso.save();
    res.json({message: "Ingreso saved"});
};

ingresosCtrl.updateDato = async (req, res) => {
    const { concepto, monto, moneda, date,  conversion_dolar, cotizacion_dolar} = req.body;
    await Ingreso.findByIdAndUpdate(req.params.id, {
        concepto: concepto,
        monto: monto,
        moneda: moneda,
        date: date,
        conversion_dolar: conversion_dolar,
        cotizacion_dolar: cotizacion_dolar
    });
    res.json({message: "dato updated"});
};

ingresosCtrl.deleteIngreso = async (req, res) => {
    await Ingreso.findByIdAndDelete(req.params.id);
    res.json({message: 'Ingreso deleted'});
};

// Ingresos en ARS
ingresosCtrl.groupIngresosARS = async (req,res) => {
    const resp = await Ingreso.aggregate([
        {
            $match: {
                idUser: req.params.idUser,
                moneda: 'ARS',
            }
        },
        {
            $group: {
                _id: '$concepto',
                monto: {$sum: '$monto'}
            }
        }
    ]);
    res.json(resp);
}

// Ingresos en USD
ingresosCtrl.groupIngresosUSD = async (req,res) => {
    const resp = await Ingreso.aggregate([
        {
            $match: {
                idUser: req.params.idUser,
                moneda: 'USD'
            }
        },
        {
            $group: {
                _id: '$concepto',
                monto: {$sum: '$monto'}
            }
        }
    ]);
    res.json(resp);
}

ingresosCtrl.getIngresosMonth = async(req,res) => {
    let date = new Date()
    let month = date.getMonth() // Obtengo mes actual

    // Validacion para que no caiga fuera de rango
    // 0 = Enero, 1 = Febrero....
    if (month === 11){
        month = 1
    } else{
        month += 1
    }

    const resp = await Ingreso.aggregate([
        {
            $addFields: {
                'month' : {$month: '$date'}
            }
        }, 
        {
            $match: {
                idUser: req.params.userId,
                moneda: req.params.id,
                month: month
            }
        },
        {
            $group: {
                _id: 'MontoMensual',
                monto: {$sum: '$monto'}
            }
        }
    ]);
    res.json(resp);
} 

module.exports = ingresosCtrl;