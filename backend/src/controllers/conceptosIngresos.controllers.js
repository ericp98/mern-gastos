const conceptosIngresoCtrl = {};

const ConceptosI = require('../models/ConceptosIngreso');

conceptosIngresoCtrl.getConceptos = async (req, res) => {
    const conceptos = await ConceptosI.find();
    res.json(conceptos);
};

conceptosIngresoCtrl.getConceptosByUser = async (req, res) => {
    const conceptos = await ConceptosI.find({id_usuario: req.params.id});
    res.json(conceptos);
};

conceptosIngresoCtrl.createConceptos = async (req, res) => {
    const { concepto, id_usuario } = req.body;
    const newConceptoI = new ConceptosI({
        id_usuario,
        concepto
    });
    await newConceptoI.save();
    res.json({message: 'ConceptoIngreso save'});
};

conceptosIngresoCtrl.deleteConcepto = async(req, res) => {
    await ConceptosI.findOneAndDelete({_id: req.params.id});
    res.json({message: "Concepto deleted"});
};

module.exports = conceptosIngresoCtrl;