const userCtrl = {};
const User = require('../models/UserModel');
const passport = require('passport');
const bcrypt = require('bcryptjs');

/* userCtrl.getUsers = async (req,res) => {
    const users = await User.find();
    res.json(users);
} */

/* userCtrl.createUser = async (req,res) => {
    const { email, name, surname, password } = req.body;
    const newUser = new User({email, name, surname, password})
    await newUser.save();
    res.json({message: "Usuario creado"})
} */

userCtrl.getTest = (req, res) => {
  res.json({res: 'asd'})
};

userCtrl.createUser = (req, res) => {
    const { email, password } = req.body;
    console.log(req.body.email);
    User.findOne({ email: req.body.email }, async (err, doc) => {
      if (err) throw err;
      if (doc) res.send({isUserValid: false}); // User already exist
      if (!doc) {
        const hashedPassword = await bcrypt.hash(password, 10);
  
        const newUser = new User({
          email: email,
          password: hashedPassword,
        });
        await newUser.save();
        console.log(newUser._id)
        res.send({isUserValid: true, idNewUser: newUser._id}); // User created
      }
    });
  };
  
userCtrl.editUser = async (req, response) => {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const userId = req.body.id;
    const userFind = await User.findOne({_id: userId });
    bcrypt.compare(oldPassword, userFind.password, function (err, res){
      if (err){
        response.json({err});
      }
      if (res){
        this.setNewPassword(newPassword, userId);
        response.json({isValid: true});
      } else {
        response.json({isValid: false});
      }
    });  
  }
  
  setNewPassword = async (pass, userId) => { 
    const hashedNewPassword = await bcrypt.hash(pass, 10);
    await User.findOneAndUpdate({_id: userId}, {password: hashedNewPassword}); 
  } 
  
  userCtrl.signin = (req, res, next) => {
    console.log(req.body.email)
    passport.authenticate("local-signin", (err, user, info) => {
      if (err) throw err;
      if (!user) {
        res.send(false)
      } else {
        req.logIn(user, (err) => {
          if (err) throw err;
          res.send({userID: req.user._id, isUserValid: true })
        });
      }
    })(req, res, next);
  }; 
  
  /* // Finaliza la sesion (Boton cerrar sesion)
  router.get('/logout', (req, res, next) => {
      req.logOut();
      res.redirect('/')
  })
  
  // Validacion para corroborar que los usuarios esten logueados
  function isAuthenticated(req, res, next) {
    if (req.isAuthenticated()){
        return true
    }
    return false
  } 
  
  router.use((req, res, next) => {
    isAuthenticated(req, res, next);
    next()
  }) 
  
  router.get('/profile', (req,res,next) => {
      res.json({message: 'asd'});
  });  */

/* userCtrl.signin = passport.authenticate('local', {
    failureRedirect: '/signin',
    successRedirect: '/inicio',
    failureFlash: true
}) */


//user.autenticarUser = () => {
    /* passport.authenticate('local-signup') , {
        successRedirect: 'http://localhost:3000/inicio', //Hardcodeado
        failureRedirect: 'http://localhost:3000/login', //Hardcodeado
        passReqToCallback: true
    } */
//}


/* passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
    const user = await User.findById(id);
    done(null, user);
});

passport.use('local-signup', new LocalStrategy({
    passReqToCallback: true 
}, async (req, done) => {

    const userCreated = User.findOne({email: req.email})
    if (userCreated){
        return done(null, false, )
    }

    const { email, name, surname, password } = req.body
    const user = new User({
        email, 
        name, 
        surname, 
        password: user.encryptPassword(password)
    });
    await user.save();
    done(null, user);
})); */

module.exports = userCtrl;

