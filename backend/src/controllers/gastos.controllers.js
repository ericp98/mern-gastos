const gastosCtrl = {};

const Gasto = require('../models/Gastos');

gastosCtrl.getGastos = async (req,res) => {
    const gastos = await Gasto.find();
    res.json(gastos);
};

gastosCtrl.getGastosUser = async (req,res) => {
    const resp = await Gasto.aggregate([
        {
            $match: {
                idUser: req.params.id
            }
        }
    ]);
    res.json(resp);
} 

gastosCtrl.createGasto = async (req, res) => {
    const { idUser, title, concepto, monto, moneda, date, conversion_dolar, cotizacion_dolar } = req.body;
    const newGasto = new Gasto({
        idUser,
        title,
        concepto,
        monto,
        moneda,
        date,
        conversion_dolar,
        cotizacion_dolar
    });
    await newGasto.save();
    res.json({message: "Gasto saved"});
};

gastosCtrl.updateDato = async (req, res) => {
    const { idUser, title, concepto, monto, moneda, date, conversion_dolar, cotizacion_dolar } = req.body;
    await Gasto.findByIdAndUpdate(req.params.id, {
        idUser, title, concepto, monto, moneda, date, conversion_dolar, cotizacion_dolar
    });
    res.json({message: "dato updated"});
};

gastosCtrl.deleteGasto = async (req, res) => {
    await Gasto.findByIdAndDelete(req.params.id);
    res.json({message: 'Gasto deleted'});
};

// Gastos en ARS
gastosCtrl.groupGastosARS = async (req,res) => {
    const resp = await Gasto.aggregate([
        {
            $match: {
                idUser: req.params.idUser,
                moneda: 'ARS'
            }
        },
        {
            $group: {
                _id: '$concepto',
                monto: {$sum: '$monto'}
            }
        }
    ]);
    res.json(resp);
}

// Gastos en USD
gastosCtrl.groupGastosUSD = async (req,res) => {
    const resp = await Gasto.aggregate([
        {
            $match: {
                idUser: req.params.idUser,
                moneda: 'USD'
            }
        },
        {
            $group: {
                _id: '$concepto',
                monto: {$sum: '$monto'}
            }
        }
    ]);
    res.json(resp);
}

gastosCtrl.getGastosMonth = async(req,res) => {
    let date = new Date()
    let month = date.getMonth() // Obtengo mes actual

    // Validacion para que no caiga fuera de rango
    // 0 = Enero, 1 = Febrero....
    if (month === 11){
        month = 1
    } else{
        month += 1
    }

    const resp = await Gasto.aggregate([
        {
            $addFields: {
                'month' : {$month: '$date'}
            }
        }, 
        {
            $match: {
                idUser: req.params.idUser,
                moneda: req.params.id,
                month: month
            }
        },
        {
            $group: {
                _id: 'MontoMensual',
                monto: {$sum: '$monto'}
            }
        }
    ]);
    res.json(resp);
} 

module.exports = gastosCtrl;