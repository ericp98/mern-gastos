const { Router } = require('express');
const router = Router();

const { createDatoF, getDatosF , deleteDato, editDato, getById, groupDatosF, editSomeDato } = require('../controllers/datosFinancieros.controllers.js');

router.route('/')
    .get(getDatosF)
    .post(createDatoF)

router.route('/datosf/:idUser')
    .get(groupDatosF)

router.route('/:id')
    .get(getById)
    .put(editDato)
    .delete(deleteDato)
    .patch(editSomeDato)

module.exports = router;