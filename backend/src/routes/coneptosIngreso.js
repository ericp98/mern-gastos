const { Router } = require('express');
const router = Router();

//Requerir funciones de controllers
const { getConceptos, createConceptos, deleteConcepto, getConceptosByUser } = require('../controllers/conceptosIngresos.controllers.js');

router.route('/')
    .get(getConceptos)
    .post(createConceptos);

router.route('/:id')
    .delete(deleteConcepto)
    .get(getConceptosByUser);

module.exports = router;