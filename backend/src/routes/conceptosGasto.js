const { Router } = require('express');
const router = Router();

// Requerir funciones del controlador
const { getConceptos, deleteConcepto, createConcepto, getConceptosByUser } = require('../controllers/conceptosGastos.controllers.js');

router.route('/')
    .get(getConceptos)
    .post(createConcepto)

router.route('/:id')
    .delete(deleteConcepto)
    .get(getConceptosByUser)

module.exports = router