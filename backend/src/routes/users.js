const { Router } = require('express');
const router = Router();

const { createUser, editUser, signin, getTest } = require('../controllers/usersControllers.controllers.js');

router.route('/')
  .get(getTest)

router.route('/signup')
  .post(createUser)

router.route('/changePassword')
  .patch(editUser)

router.route('/signin')
  .post(signin)
  

module.exports = router; 

/* const passport = require('passport');
const User = require('../models/user');
const bcrypt = require('bcryptjs');

router.post("/signup", (req, res) => {
  User.findOne({ email: req.body.email }, async (err, doc) => {
    if (err) throw err;
    if (doc) res.send({isUserValid: false}); // User already exist
    if (!doc) {
      const hashedPassword = await bcrypt.hash(req.body.password, 10);

      const newUser = new User({
        email: req.body.email,
        password: hashedPassword,
      });
      await newUser.save();
      console.log(newUser._id)
      res.send({isUserValid: true, idNewUser: newUser._id}); // User created
    }
  });
});

router.patch("/changePassword", async (req, response) => {
  const oldPassword = req.body.oldPassword;
  const newPassword = req.body.newPassword;
  const userId = req.body.id;
  const userFind = await User.findOne({_id: userId });
  bcrypt.compare(oldPassword, userFind.password, function (err, res){
    if (err){
      response.json({err});
    }
    if (res){
      this.setNewPassword(newPassword, userId);
      response.json({isValid: true});
    } else {
      response.json({isValid: false});
    }
  });  
})

setNewPassword = async (pass, userId) => { 
  const hashedNewPassword = await bcrypt.hash(pass, 10);
  await User.findOneAndUpdate({_id: userId}, {password: hashedNewPassword}); 
} 

router.post("/signin", (req, res, next) => {
  passport.authenticate("local-signin", (err, user, info) => {
    if (err) throw err;
    if (!user) {
      res.send(false)
    } else {
      req.logIn(user, (err) => {
        if (err) throw err;
        res.send({userID: req.user._id, isUserValid: true })
      });
    }
  })(req, res, next);
}); 

// Finaliza la sesion (Boton cerrar sesion)
router.get('/logout', (req, res, next) => {
    req.logOut();
    res.redirect('/')
})

// Validacion para corroborar que los usuarios esten logueados
function isAuthenticated(req, res, next) {
  if (req.isAuthenticated()){
      return true
  }
  return false
} 

router.use((req, res, next) => {
  isAuthenticated(req, res, next);
  next()
}) 

router.get('/profile', (req,res,next) => {
    res.json({message: 'asd'});
});  */