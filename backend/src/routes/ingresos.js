const { Router } = require('express');
const router = Router();

const { getIngresos, updateDato, createIngreso, deleteIngreso, groupIngresosARS, groupIngresosUSD, getIngresosMonth, getIngresosUser } = require('../controllers/ingresos.controllers.js');

router.route('/')
    .get(getIngresos)
    .post(createIngreso)

router.route('/ingresos_ARS/:idUser')
    .get(groupIngresosARS)

router.route('/ingresos_USD/:idUser')
    .get(groupIngresosUSD)

router.route('/:id')
    .delete(deleteIngreso)
    .put(updateDato)
    .get(getIngresosUser)

router.route('/:id/:userId')
    .get(getIngresosMonth)

module.exports = router;