const { Router } = require('express');
const router = Router();

const { createGasto, deleteGasto, getGastos, updateDato, groupGastosARS, groupGastosUSD, getGastosMonth, getGastosUser } = require('../controllers/gastos.controllers.js');

router.route('/')
    .get(getGastos)
    .post(createGasto)

router.route('/gastos_ARS/:idUser')
    .get(groupGastosARS)

router.route('/gastos_USD/:idUser')
    .get(groupGastosUSD)

router.route('/:id')
    .put(updateDato)
    .delete(deleteGasto)
    .get(getGastosUser)

router.route('/:id/:idUser')
    .get(getGastosMonth)

module.exports = router;