const { Schema, model } = require('mongoose');

const conceptoIngreso = new Schema({
    id_usuario: String,
    concepto: {
        type: String, 
        unique: true
    }
});

module.exports = model('ConceptoIngreso', conceptoIngreso);