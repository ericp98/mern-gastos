const { Schema, model } = require('mongoose');

const conceptoGasto = new Schema({
    id_usuario: String,
    concepto: {
        type: String, 
        unique: true
    }
});

module.exports = model('ConceptoGasto', conceptoGasto);