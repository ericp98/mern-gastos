const { Schema, model } = require('mongoose');

const datoSchema = new Schema ({
    id_usuario: String,
    saldo_ARS: Number,
    saldo_USD: Number,
    cotizacion_USD: Number,
    saldoARS_toUSD: Number,
    saldoUSD_toARS: Number
});

module.exports = model('datoFinanciero', datoSchema);