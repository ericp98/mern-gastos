const { Schema, model } = require('mongoose');

const ingresoSchema = new Schema({
    idUser:{
        type:String,
        required: true
    },
    title: String,
    concepto: {
        type: String,
        required: true
    },
    monto: {
        type: Number,
        required: true
    },
    moneda: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    conversion_dolar: {
        type: Number,
        required: true
    },
    cotizacion_dolar: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
});

module.exports = model('Ingreso', ingresoSchema);